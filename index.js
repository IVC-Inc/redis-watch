"use strict";

const heartbeats = require('heartbeats');
const uuid = require('node-uuid');
const Redis = require('ioredis');

let heart = heartbeats.createHeart(1000);
let inProgress = {};

function monitor(redis) {

  // Every 10 seconds, set a key with a UUID and then attempt to get it
  let launchEvent = heart.createEvent(10,function(hb,last) {
    let testUUID = `redis-watch:${uuid.v4()}`;

    inProgress[testUUID] = true;

    redis.set(testUUID,true, 'EX', 9); // TTL of 9 seconds
    redis.get(testUUID,function(err,value) {
      if(!err && value) delete inProgress[testUUID];
    });

    // 3 seconds from now, check to see if we're still looking for the test key
    let testEvent = heart.createEvent(3,{ repeat: 1 },function(hb,last) {
       if(inProgress[testUUID]) {
         console.log(`Error: redis test timeout ${testUUID} still in progress after 3 seconds`);
         delete inProgress[testUUID];
       }
    });
  });
}

module.exports = function(args) {
  args.dropBufferSupport = true;  
  let redis = new Redis(args);

  redis.on("error",function(err) {
    let msg = `Error: redis error: ${err}`
    console.log(msg);
  });

  return { 
    monitor:function() { monitor(redis); }
  }
}
